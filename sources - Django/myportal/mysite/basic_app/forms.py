from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Profile


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=150, required=False, label='Όνομα',
                                 help_text='Πρέπει να είναι από 150 χαρακτήρες και κάτω.'
                                           '.Γράμματα μόνο.')
    last_name = forms.CharField(max_length=150, required=False, label='Επίθετο',
                                help_text='Πρέπει να είναι από 150 χαρακτήρες και κάτω.'
                                          '.Γράμματα μόνο.')
    email = forms.EmailField(max_length=254, help_text='Υποχρεωτικό. Γράψτε ένα έγκυρο email')
    username = forms.CharField(label='Συνθηματικό Χρήστη',
                               help_text='Υποχρεψτικό πεδίο. Πρέπει να είναι από 150 χαρακτήρες και κάτω.'
                                         '.Γράμματα, αριθμοί και @/./+/-/_. μόνο.')
    password1 = forms.CharField(label="Κωδικός Χρήστη", widget=forms.PasswordInput,
                                help_text='To password δεν πρέπει να είναι όμοιω με τις παραπάνω πληροφορίες. '
                                          ' Το password πρέπει να περιέχει το λιγότερο 8 χαρακτήρες. '
                                          'Πρέπει να περιέχει τουλάχιστον ένα κεφαλάιο και ένα μικρό γράμμα και έναν αριθμό')
    password2 = forms.CharField(label="Επαλήθευση Κωδικού", widget=forms.PasswordInput,
                                help_text='Γράψτε τον ίδιο Κωδικός Χρήστη που γράψατε από πάνω ')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password1', 'password2')


class ProfileForm(ModelForm):
    afm = forms.CharField(max_length=9, required=False, label="ΑΦΜ", help_text='Υποχρεωτικό. Γράψτε ένα έγκυρο ΑΦΜ')

    class Meta:
        model = Profile
        fields = ['afm']
