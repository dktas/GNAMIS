import hashlib

from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from .forms import ProfileForm
from .forms import SignUpForm
from .models import Profile


@login_required
def index(request):
    return render(request, 'index.html')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        profile_form = ProfileForm(request.POST)
        if form.is_valid():
            if profile_form.is_valid():
                form.save()
                profile_form.save(commit=False)
                afm = profile_form.cleaned_data.get('afm')
                username = form.cleaned_data.get('username')
                str_afm = str(afm)
                tokenafm = hashlib.sha256(str_afm.encode()).hexdigest()
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=username, password=raw_password)
                login(request, user)
                Profile.objects.create(afm=afm, user_id=user.id, tokenafm=tokenafm)

        return redirect('index')
    else:
        form = SignUpForm()
        profile_form = ProfileForm()
    return render(request, 'login/signup.html', {'form': form, 'profile_form': profile_form})
