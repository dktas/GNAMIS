from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.ForeignKey(User)
    id = models.AutoField(primary_key=True)
    afm = models.CharField(max_length=10, blank=False, unique=True)
    tokenafm = models.CharField(max_length=256)

    def __unicode__(self):
        return self.afm

