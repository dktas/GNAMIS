from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.cars_list),
    url(r'^cars_list/', views.cars_list, name='cars_list'),
    url(r'^new_insur', views.new_insur, name='new_insur'),
    url(r'^vih_user_list/', views.vih_user_list, name='vih_user_list'),
    url(r'^detail_list/', views.detail_list, name='detail_list'),
    url(r'^detail_list_insur/', views.detail_list_insur, name='detail_list_insur'),
    url(r'^my_model/', views.my_model, name='my_model'),
    url(r'^new_cost', views.new_cost, name='new_cost'),
    url(r'^new_cost_year', views.new_cost_year, name='new_cost_year'),
]
