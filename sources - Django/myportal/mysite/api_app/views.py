import hashlib
import requests
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.template.defaultfilters import length

from basic_app.models import Profile


# Create your views here.
@login_required
def cars_list(request):
    return render(request, 'apiapp/carsDetails.html')  # , context)


@login_required
def my_model(request):
    return render(request, 'apiapp/myModel.html')  # , context)


@login_required
def vih_user_list(request):
    afm_num = get_afm(request)
    tokenafm = hashlib.sha256(afm_num.encode()).hexdigest()
    web_data = requests.get(url="http://www.nodeserver.space/afmcars/" + afm_num + "/" + tokenafm)
    data = web_data.json()
    for record in data:
        record["id"] = record.pop("ownerAFM")
        context = {'data': data, 'url_redirect': 'api_app'}
    return render(request, 'apiapp/mycars.html', context)


@login_required
def new_cost(request):
    plate = str(request.GET.get("plate"))
    month = str(request.GET.get("month"))
    web_data = requests.get(url="http://www.nodeserver.space/getprice/" + plate + "/" + month)
    data1 = web_data.json()
    context = {'data1': data1, 'url_redirect': 'api_app'}
    return render(request, 'apiapp/mycarsins.html', context)

@login_required
def new_cost_year(request):
    plate = str(request.GET.get("plate"))
    month = str(request.GET.get("month"))
    web_data = requests.get(url="http://www.nodeserver.space/getprice/" + plate + "/" + month)
    data1 = web_data.json()
    context = {'data1': data1, 'url_redirect': 'api_app'}
    return render(request, 'apiapp/mycarsins.html', context)

@login_required
def detail_list(request):
    plate = str(request.GET.get("name"))
    afm_num = get_afm(request)
    tokenafm = hashlib.sha256(afm_num.encode()).hexdigest()
    web_data = requests.get(url="http://www.nodeserver.space/infoplate/" + plate + "/" + tokenafm)
    data = web_data.json()
    plate = data['accidents']
    count = get_count_accidents(request)
    for i in count:
        plate
    context = {'data': data, 'plate': plate, 'url_redirect': 'api_app'}
    return render(request, 'apiapp/mycardetail.html', context)


@login_required
def new_insur(request):
    plate = str(request.GET.get("plate"))

    afm_num = get_afm(request)
    tokenafm = hashlib.sha256(afm_num.encode()).hexdigest()
    post_data = {'plate': plate, 'type': 6, 'token': tokenafm}
    headers = {'Content-Type': 'application/json'}
    url = "http://www.nodeserver.space/insurancerequest"
    requests.post(url, json=post_data, headers=headers)

    context = {'url_redirect': 'api_app'}
    return render(request, 'apiapp/mycars.html', context)


@login_required
def detail_list_insur(request):
    plate = str(request.GET.get("name"))
    afm_num = get_afm(request)
    tokenafm = hashlib.sha256(afm_num.encode()).hexdigest()
    web_data = requests.get(url="http://www.nodeserver.space/insurancesplate/" + plate + "/" + tokenafm)
    data = web_data.json()
    count = get_count_insur(request)
    for i in count:
        data
    context = {'data': data, 'plate': plate, 'url_redirect': 'api_app'}
    return render(request, 'apiapp/insuranhistory.html', context)


@login_required
def get_afm(request):
    afm_user = (Profile.objects.filter(user=request.user).values_list('afm', flat=True))
    return str(afm_user[0])


@login_required
def get_count_accidents(request):
    plate = str(request.GET.get("name"))
    afm_num = get_afm(request)
    tokenafm = hashlib.sha256(afm_num.encode()).hexdigest()
    web_data = requests.get(url="http://www.nodeserver.space/infoplate/" + plate + "/" + tokenafm)
    data = web_data.json()
    accidents = data["accidents"]
    countd = str(length(accidents))
    return str(countd)


@login_required
def get_count_insur(request):
    plate = str(request.GET.get("name"))
    afm_num = get_afm(request)
    tokenafm = hashlib.sha256(afm_num.encode()).hexdigest()
    web_data = requests.get(url="http://www.nodeserver.space/insurancesplate/" + plate + "/" + tokenafm)
    data = web_data.json()
    countd = str(length(data))
    return str(countd)
